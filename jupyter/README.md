# Jupyter Notebooks
As someone with a lot of experience with Wolfram Mathematica at both low and high levels, I do very much enjoy the notebook experience in my development cycle.

Here are five files and their purposes:

- `Run Experiment`: runs the sacred experiment and at the end shows how one can use the model via `tf.contrib.predictor` (e.g. if one wanted to set up an API with Flask)
- `Eval`: loads ands evaluates the pruned models
- `playground - reading exported estimator`: a place to test code for ensuring models are read in correctly
- `playground - using altered estimator models`: due to the nature of estimator apis, exported models can be easily imported, but not updated and exported. So this is a place for testing the loading of these altered models
- `All Together`: the relevant code from all notebooks and the `forai` python module required to train a model, prune it, evaluate the pruned models, and chart the results

again, these files require that either `forai` is installed locally `pip install -e path/to/<repo>` or `forai` is added to the system path `sys.path.insert('path/to/<repo>/', 0)`
