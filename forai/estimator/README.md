# estimator submodule
This directory consists of the submodule for constructing a
[`tf.estimator`][estimator].

Given the complexity and the length some functions can obtained, relevant chunks
have broken up into the following files:

- `build_fn.py`: this file contains the functions necessary to "wire" the graph
- `exporter.py`: this file contains any and all exporters for saving the estimator
- `exports_fn.py`: this file contains the function specifying what values are
exported from the extimator.
- `input_fn.py`: this file contains the functions necessary to produce the input
for the estimator in three contexts: "train", "eval", and "test"
- `loss_fn.py`: defines how the loss of this model is calculated
- `metrics.py`: metrics to be observed in `tensorboard` as well as produced by
the estimator
- `mode_eval.py`: handles logic of the estimator when mode is "eval"
- `mode_predict.py`: handles logic of the estimator when mode is "predict"
- `mode_train.py`: handles logic of the estimator when mode is "train"
- `model_fn.py`: the expected function needed to construct an `estimator`. Handles
the logic of building the graph, evaluating and routing logic depending on mode.
- `serving_input_receiver_fn.py`: the `serving_input_receiver_fn` needed for exporting
the model and using the trained estimator as a predictor. 


[estimator]: https://www.tensorflow.org/guide/estimators
