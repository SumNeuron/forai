import os, sys, tensorflow as tf, numpy as np

def loss_fn(MODEL):
    """How to calculate the loss of the MODEL.

    Args:
        MODEL (dict): a `dict` containing the MODEL

    Returns:
        MODEL (dict): an updated `dict` containing the loss of the MODEL

    """

    # here you extract what you need to calculate the loss
    logits = MODEL['logits']
    labels = MODEL['labels']

    # depending on batch size NaN loss can be achieved so add  a small amount
    # to prevent that
    epsilon = tf.constant(value=0.00001, shape=logits.shape)

    # here you calculate the loss
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels, logits+epsilon))

    # add loss to MODEL
    MODEL['loss'] = loss
    return MODEL
