import os, sys, tensorflow as tf, numpy as np
def metrics_fn(MODEL):
    """Produce metrics of the MODEL to monitor during training.

    Args:
        MODEL (dict): a `dict` containing the MODEL

    Returns:
        MODEL (`dict`): an update `dict` containg the metrics

    .. _EstimatorSpec:
        https://www.tensorflow.org/api_docs/python/tf/estimator/EstimatorSpec

    """
    _run = MODEL['params']['_run'] if "_run" in MODEL['params'] else None


    # here you extract variables for easier reading
    predicted     = MODEL['predictions']
    labels        = MODEL['labels']
    categories    = MODEL['categories']

    logits        = predicted['logits']
    probabilities = predicted['probabilities']

    # here you calculate your metrics
    mae = tf.metrics.mean_absolute_error(labels=labels, predictions=probabilities, name='mea_op')
    mse = tf.metrics.mean_squared_error(labels=labels, predictions=probabilities, name='mse_op')
    true_categoires = tf.argmax(MODEL['labels'], axis=1)
    acc = tf.metrics.accuracy(true_categoires, categories)

    # here you add the above metrics to the MODEL
    metrics = {
        'mae': mae, 'mse': mse, 'acc': acc
    }

    if _run is not None:
        _run.log_scalar('mae', mae)
        _run.log_scalar('mse', mse)
        _run.log_scalar('acc', acc)

    MODEL['metrics'] = metrics
    return MODEL
