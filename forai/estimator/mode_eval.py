import os, sys, tensorflow as tf, numpy as np

def mode_eval(MODEL:dict):
    """How to evaluate the MODEL.

    Args:
        MODEL (dict): a `dict` containing the MODEL

    Returns:
        spec (`EstimatorSpec`_): Ops and objects returned from a MODEL_fn and passed to an Estimator

    .. _EstimatorSpec:
        https://www.tensorflow.org/api_docs/python/tf/estimator/EstimatorSpec

    """
    # do additional evaluation here

    spec = tf.estimator.EstimatorSpec(
        mode            = MODEL['mode'],
        loss            = MODEL['loss'],
        eval_metric_ops = MODEL['metrics'],
        predictions     = MODEL['predictions'],
        export_outputs  = MODEL['export_outputs']
    )
    return spec
