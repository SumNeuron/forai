import os, sys, tensorflow as tf, numpy as np

def serving_input_receiver_fn():
    flat_mnist_tensor = tf.placeholder(tf.float32, (None, 784), name='feature')

    # this is the dict that is then passed as "features" parameter to model_fn
    features = {"flat_mnist": flat_mnist_tensor}

    # This is needed to map the input to a name that can be retrieved later
    receiver_tensors = features
    return tf.estimator.export.ServingInputReceiver(features, receiver_tensors)
