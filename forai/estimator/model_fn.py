import os, sys, tensorflow as tf, numpy as np

from .build_fn     import build_fn
from .exports_fn   import exports_fn
from .metrics_fn   import metrics_fn
from .mode_predict import mode_predict
from .mode_eval    import mode_eval
from .mode_train   import mode_train
from .loss_fn      import loss_fn



def model_fn(features, labels, mode, params):
    '''
    model_fn: Model function. Follows the signature:

    Args:
        features: This is the first item returned from the input_fn passed to train, evaluate, and predict. This should be a single Tensor or dict of same.
        labels: This is the second item returned from the input_fn passed to train, evaluate, and predict. This should be a single Tensor or dict of same (for multi-head models). If mode is ModeKeys.PREDICT, labels=None will be passed. If the model_fn's signature does not accept mode, the model_fn must still be able to handle labels=None.
        mode: Optional. Specifies if this training, evaluation or prediction. See ModeKeys.
        params: Optional dict of hyperparameters. Will receive what is passed to Estimator in params parameter. This allows to configure Estimators from hyper parameter tuning.
        config: Optional configuration object. Will receive what is passed to Estimator in config parameter, or the default config. Allows updating things in your model_fn based on configuration such as num_ps_replicas, or model_dir.

    Returns: EstimatorSpec
    '''
    MODEL = {'features': features, 'labels': labels, 'mode': mode, 'params': params}

    # send the features through the graph
    MODEL = build_fn(MODEL)

    # define what gets exported
    MODEL = exports_fn(MODEL)

    # prediction
    if mode == tf.estimator.ModeKeys.PREDICT: return mode_predict(MODEL)

    # calculate the loss
    MODEL = loss_fn(MODEL)

    # calculate all metrics and send them to tf.summary
    MODEL = metrics_fn(MODEL)

    # evaluation
    if mode == tf.estimator.ModeKeys.EVAL: return mode_eval(MODEL)

    # train
    if mode == tf.estimator.ModeKeys.TRAIN: return mode_train(MODEL)
