import os, sys, tensorflow as tf, numpy as np

def exports_fn(MODEL:dict):
    """What to export from the MODEL.

    Args:
        MODEL (dict): a `dict` containing the MODEL

    Returns:
        MODEL (`dict`): an update `dict` containg the metrics
    """
    # calculate all predictions here
    MODEL['predictions'] = {
        'probabilities': MODEL['probabilities'],
        'logits': MODEL['logits'],
        'categories': MODEL['categories'],
    }

    # define exports_fn
    MODEL['export_outputs'] = {'predictions': tf.estimator.export.PredictOutput(MODEL['predictions'])}
    return MODEL
