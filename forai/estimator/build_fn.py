import os, sys, tensorflow as tf, numpy as np

def build_fn(MODEL:dict) -> dict:
    """How to wire (build the computational graph) of the MODEL.
    Args:
        MODEL (dict): a `dict` containing the MODEL

    Returns:
        MODEL (dict): an updated `dict` containing the MODEL
    """
    # here we extract what is needed for building the graph
    features = MODEL['features']
    params   = MODEL['params']

    h_layers = params['hidden_layers']

    # here you wire how features go throught the graph
    x = features[params["input_feature"]]
    # input_shape  = (None, int(x.shape[-1]))
    # output_shape = (batch_size, len(params['classes']))
    # x.set_shape(input_size)

    x = tf.layers.dense(x, h_layers[0], name='input_layer',reuse=tf.AUTO_REUSE)
    for i in range(1, len(h_layers)):
        x = tf.layers.dense(x, h_layers[i], name='hidden_layer_{}'.format(i), reuse=tf.AUTO_REUSE)
    x = tf.layers.dense(x, len(params['classes']), name='output_layer')

    # here you store the outputs of the graph
    MODEL['logits'] = x # loss automatically applied the sigmoid to it
    MODEL['probabilities']  = tf.nn.softmax(x) # needed for prediction
    MODEL['categories']  = tf.argmax(MODEL['probabilities'], axis=1) # needed for class identification
    return MODEL
