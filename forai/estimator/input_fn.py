import os, sys, random, operator, tensorflow as tf, numpy as np

def input_fn(params:dict, _log=None):
    """How to feed the MODEL.

    This input function builds an input pipeline that yields batches of
    (features, labels) pairs, where features is a dictionary features.

    Args:
        params (dict): a `dict` containing any optional specifiers
        _log (logger): optional python logger. Default None.

    Returns:
        data (tuple): batches of (features, labels) pairs which are passed to
            `MODEL_fn` as the corresponding arguments


    """
    # extract the mode
    mode = params['mode'] if 'mode' in params else 'train'
    if _log is not None: _log.debug('setting up input_fn in [mode={}]'.format(mode))

    if _log is not None: _log.debug('loading mnist from tf.keras')
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
    if _log is not None: _log.debug('mnist data retrieved')

    features = x_train if mode == 'train' else x_test
    labels = y_train if mode == 'train' else y_test

    # make the dataset from TF Record files
    if _log is not None: _log.debug('converting (features, labels) tuple to tf.data.Dataset')
    dataset = tf.data.Dataset.from_tensor_slices(
    {
        "features": features.reshape(features.shape[0], operator.mul(*features.shape[1:])).astype('float32'),
        "labels":   tf.one_hot(labels, len(params['classes']))
    })
    # dataset should be a tuple of (features, labels)
    dataset = dataset.map(lambda record: ({"flat_mnist": record["features"]}, record["labels"]))

    # under training, repeat examples if needed
    if mode == 'train':
        dataset = dataset.repeat()

    if mode in ['train', 'eval']:
        dataset = dataset.apply(tf.contrib.data.batch_and_drop_remainder(params['batch_size']))

    return dataset.make_one_shot_iterator().get_next()
