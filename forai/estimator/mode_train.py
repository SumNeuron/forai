import os, sys, tensorflow as tf, numpy as np

def mode_train(MODEL):
    """How to train the MODEL.

    Args:
        MODEL (dict): a `dict` containing the MODEL

    Returns:
        spec (`EstimatorSpec`_): Ops and objects returned from a MODEL_fn and passed to an Estimator

    .. _EstimatorSpec:
        https://www.tensorflow.org/api_docs/python/tf/estimator/EstimatorSpec

    """
    # extract variables for easier reading here
    global_step   = tf.train.get_global_step()
    learning_rate = MODEL['params']['learning_rate']
    momentum      = MODEL['params']['momentum']
    decay         = MODEL['params']['decay']
    loss          = MODEL['loss']

    # do the training here
    # MODEL['optimizer'] = tf.train.RMSPropOptimizer(learning_rate=learning_rate, decay=decay)
    MODEL['optimizer'] = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=momentum)
    MODEL['train_op'] = MODEL['optimizer'].minimize(loss, global_step=global_step)

    spec = tf.estimator.EstimatorSpec(
        mode            = MODEL['mode'],
        loss            = MODEL['loss'],
        train_op        = MODEL['train_op'],
        eval_metric_ops = MODEL['metrics'],
        predictions     = MODEL['predictions'],
        export_outputs  = MODEL['export_outputs']
    )
    return spec
