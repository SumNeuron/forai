import os, sys, tensorflow as tf, numpy as np


from .serving_input_receiver_fn import serving_input_receiver_fn

exporter = tf.estimator.BestExporter(
    name="best_exporter",
    serving_input_receiver_fn=serving_input_receiver_fn,
    exports_to_keep=5
) # this will keep the 5 best checkpoints
