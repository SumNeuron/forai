import os

'''-----------------------------------------------------------------------------
DIRECTORIES AND FILES
-----------------------------------------------------------------------------'''
MODULE_DIR      = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR     = os.path.join(MODULE_DIR, '..')

JUYPTER_DIR     = os.path.join(PROJECT_DIR, 'juypter')
EXPERIMENTS_DIR = os.path.join(PROJECT_DIR, 'experiments')

DEFAULT_CONFIG_FILE = os.path.join(MODULE_DIR, 'config.json')
