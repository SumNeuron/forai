import os, sys, json, numpy as np, tensorflow as tf
from mag.py.os import dir_dirs
from forai.settings import EXPERIMENTS_DIR
from forai.utils.estimator import (
    load_estimator_graph, lazy_set_variable_values, lazy_fetch_variable_values,
    estimator_weights_names, estimator_weights_names_for_pruning,
    final_model_dir, pruning_dir, save_session,
    available_checkpoints, get_checkpoint_files
)
from forai.utils.matrices import prune_k_percent, prune_method
from forai.estimator import model_fn, input_fn, serving_input_receiver_fn

def __prune_estimator(experiment_number:int, percent:float, method:str='weight_pruning'):
    '''
    Notes:
        Similar to _prune_estimator. Seems that SavedModels can not be used.
        Using checkpoint directly.

    Arguments:
        experiment_number (int): the number of the experiment corresponding to
            the sacred run._id.
        percent (float): the k percent prunned by the model.
        method (str / int): the method by which the model was prunned. By
            default set to "weight_pruning" Valid options include:
                0: "weight_pruning"
                1: "unit_pruning"

    Returns:
        None
    '''
    method = prune_method(method)
    config_file = os.path.join(EXPERIMENTS_DIR, str(experiment_number), 'config.json')


    model_dir = dir_dirs(os.path.join(EXPERIMENTS_DIR, str(experiment_number), 'final_model'))[0]
    export_dir = pruning_dir(experiment_number, percent, method)
    if not os.path.isdir(export_dir): os.makedirs(export_dir)

    with open(config_file, 'r') as f:
        config = json.load(f)

    weight_matrices_names_to_prune = estimator_weights_names_for_pruning(
        estimator_weights_names(config['hidden_layers'])
    )


    tf.reset_default_graph()
    checkpoint = tf.train.get_checkpoint_state(os.path.join(EXPERIMENTS_DIR, str(experiment_number)))
    init_op = tf.initialize_all_variables()
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(checkpoint.model_checkpoint_path + '.meta')
        saver.restore(sess, checkpoint.model_checkpoint_path)
        # refs to variables to change
        weight_matrices_ref = [
            [v for v in tf.trainable_variables() if v.name == layer_weight_name][0]
            for layer_weight_name in weight_matrices_names_to_prune
        ]

        # actual values of the variables
        weight_matrices = [sess.run(wm) for wm in weight_matrices_ref]

        # pruning weights
        updated_weights = prune_k_percent(weight_matrices, percent, method)

        # update operations
        updated_weight_matrices = [
            weight_matrices_ref[i].assign(updated_weights[i])
            for i in range(len(updated_weights))
        ]

        # have session assign new values
        for wm in updated_weight_matrices:
            sess.run(wm)

        saver.save(sess, os.path.join(export_dir, 'estimator'))






def _prune_estimator(experiment_number:int, percent:float, method:str='weight_pruning'):
    '''
    Notes:
        Similar to prune_estimator. Seems that seperating load and save of
        estimator into different functions interferes with what is actually
        saved. This probably could be fixed by passing the sessions object
        between the functions.

    Arguments:
        experiment_number (int): the number of the experiment corresponding to
            the sacred run._id.
        percent (float): the k percent prunned by the model.
        method (str / int): the method by which the model was prunned. By
            default set to "weight_pruning" Valid options include:
                0: "weight_pruning"
                1: "unit_pruning"

    Returns:
        None
    '''
    method = prune_method(method)
    config_file = os.path.join(EXPERIMENTS_DIR, str(experiment_number), 'config.json')


    model_dir = dir_dirs(os.path.join(EXPERIMENTS_DIR, str(experiment_number), 'final_model'))[0]
    export_dir = pruning_dir(experiment_number, percent, method)
    if not os.path.isdir(export_dir): os.makedirs(export_dir)

    with open(config_file, 'r') as f:
        config = json.load(f)

    weight_matrices_names_to_prune = estimator_weights_names_for_pruning(
        estimator_weights_names(config['hidden_layers'])
    )


    tf.reset_default_graph()
    init_op = tf.initialize_all_variables()
    with tf.Session(graph=tf.Graph()) as sess:

        # fetch graph
        meta_graph = tf.saved_model.loader.load(sess, ['serve'], model_dir)

        # load graph
        loaded_graph = tf.train.import_meta_graph(meta_graph)

        # sess.run(init_op)

        # refs to variables to change
        weight_matrices_ref = [
            [v for v in tf.trainable_variables() if v.name == layer_weight_name][0]
            for layer_weight_name in weight_matrices_names_to_prune
        ]

        # actual values of the variables
        weight_matrices = [sess.run(wm) for wm in weight_matrices_ref]

        # pruning weights
        updated_weights = prune_k_percent(weight_matrices, percent, method)

        # update operations
        updated_weight_matrices = [
            weight_matrices_ref[i].assign(updated_weights[i])
            for i in range(len(updated_weights))
        ]

        # have session assign new values
        for wm in updated_weight_matrices:
            sess.run(wm)


        loaded_graph.save(sess, os.path.join(export_dir, 'estimator'))

def prune_estimator(experiment_number:int, percent:float, method:str='weight_pruning') -> None:
    '''
    Arguments:
        experiment_number (int): the number of the experiment corresponding to
            the sacred run._id.
        percent (float): the k percent prunned by the model.
        method (str / int): the method by which the model was prunned. By
            default set to "weight_pruning" Valid options include:
                0: "weight_pruning"
                1: "unit_pruning"

    Returns:
        None
    '''
    method = prune_method(method)
    config_file = os.path.join(EXPERIMENTS_DIR, str(experiment_number), 'config.json')
    with open(config_file, 'r') as f:
        config = json.load(f)

    weight_matrices_names_to_prune = estimator_weights_names_for_pruning(
        estimator_weights_names(config['hidden_layers'])
    )

    # tf.reset_default_graph()
    load_estimator_graph(final_model_dir(experiment_number))

    weights_matrices = lazy_fetch_variable_values(weight_matrices_names_to_prune)
    matrices = list(weights_matrices.values())
    pruned = prune_k_percent(matrices, percent, method)
    lazy_set_variable_values(dict(zip(list(weights_matrices.keys()), pruned)))

    export_dir = pruning_dir(experiment_number, percent, method)
    save_session(export_dir)

def eval_pruned_estimator(experiment_number:int, percent:float, method:str='weight_pruning') -> dict:
    '''
    Arguments:
        experiment_number (int): the number of the experiment corresponding to
            the sacred run._id.
        percent (float): the k percent prunned by the model.
        method (str / int): the method by which the model was prunned. By
            default set to "weight_pruning" Valid options include:
                0: "weight_pruning"
                1: "unit_pruning"

    Returns:
        results (dict): the values that the estimator exports
    '''
    method = prune_method(method)
    config_file = os.path.join(EXPERIMENTS_DIR, str(experiment_number), 'config.json')
    with open(config_file, 'r') as f:
        config = json.load(f)

    pruned_dir = pruning_dir(experiment_number, percent, method)
    tf.reset_default_graph()
    est = tf.estimator.Estimator(
        model_fn  = model_fn,
        # model_dir = final_model_dir(experiment_number),
        model_dir = pruned_dir,
        config    = tf.estimator.RunConfig(**config['RunConfig']),
        params    = config,
        warm_start_from = pruned_dir
    )
    eval_fn = lambda : input_fn({**config, 'mode':'eval'})
    results = est.evaluate(eval_fn)

    est.export_savedmodel(os.path.join(pruned_dir, 'final_model'), serving_input_receiver_fn)
    return results
