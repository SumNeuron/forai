# Utils

The utils sub module consists of just that! Namely:

- `estimator.py` has several functions for handling the reading, updating and writing of estimator models. Since the `tf.estimator` API was never really finished and tensorflow is migrating to the keras API for version 2, many of the estimator contributors do not feel like helping out either! Fun times in the source code to piece this together

- `matrices.py` houses the utility functions for pruning. Some of these, which are not specific to this project, will be migrated and added my personal magazine of utility functions [`mag`][mag]

- `pruning.py` is the "pipeline" interweaving the utilities of `estimator.py` and `matrices.py`. This helps ensure that while loading and exporting graphs, tensorflow isn't stepping on its own toes



[mag]: https://pypi.org/project/mag/
