import numpy as np


def prune_method(method:str)->str:
    '''
    Helper function for handling str / int prune method argument

    Arguments:
        method (str / int): the method by which the model was prunned. Valid
            options include:
                0: "weight_pruning"
                1: "unit_pruning"
    Returns:
        method (str): a valid pruning method
    '''
    VALID_PRUNING_METHODS = ['weight_pruning', 'unit_pruning']
    if method == 0: method = 'weight_pruning'
    if method == 1: method = 'unit_pruning'
    if method not in VALID_PRUNING_METHODS:  method = 'weight_pruning'
    return method


def unravel_merged_matrices(merged_flattened_index:int, elements_in_matrices:list):
    '''
    Arguments:
        merged_flattened_index (int): the index of the element in the merged,
            flattened matrix
        elements_in_matrices (list): a list of integers corresponding to the
            number of elements in the matrix at the corresponding index
    Returns:
        flattened_index (int): the flattened index for the matrix to which
            merged_flattened_index belongs to
        matrix_index (int): the index correspoinding to the matrix to which the
            flattened_index belongs to

    '''
    flattened_index = merged_flattened_index
    matrix_index = -1
    for i, num_elements in enumerate(elements_in_matrices):
        if flattened_index < num_elements:
            matrix_index = i
            break
        flattened_index -= num_elements
    return (flattened_index, matrix_index)

def percent_to_int(percent:float, array:list):
    '''
    Arguments:
        percent (float): the percent of elements requested
        array (list): a 1d array from which perecent elements are requested
    Returns:
        n (int): the number of elements
    '''
    return int(np.round(percent * len(array)))


def merge_matrices(matrices:list):
    '''
    Arguments:
        matrices (list): a list of matrices (np.ndarray) to merge
    Returns:
        merged (list): a 1D np.ndarray consist of all the elements from matrices
        flattened_matrices (list): a list of 1D np.ndarrays corresponding to the
            flattened matrices from matrices
        elements_per_matrix (list): the number of elements per matrix in
            matrices
    '''
    # dealing with matrices of different shape is cumbersome, so flatten them
    # and leverage 1D mapping.
    flattened_matrices = [mat.flatten() for mat in matrices]

    # save some re compuation
    elements_per_matrix = list(map(len, flattened_matrices))

    # one long, flattened matrix
    merged = np.concatenate(flattened_matrices)
    return merged, flattened_matrices, elements_per_matrix

def matrix_indices_from_merged_array(
    elements_needed:int,
    merged_array:list,
    elements_per_array:list
):
    '''
    Arguments:
        elements_needed (int): how many elements to retrieve

        merged_array (list): a 1D numpy.ndarray that consists of several
            distinct n-dimensional matrices which were flattened and
            concatenate and joined together.

        elements_per_array (list): a list of the total number of elements in
            each of the n-dimensional matrices which make up the merged_array

    Returns:
        raveled_index_matrix_tuples (list): a list of tuples which consists of
            raveled_index (int): the raveled (flattened) index of the element
            matrix_index (int): the matrix to which the raveled_index belongs
                from which merged_array is made from.
    '''
    # argpartition is faster than argmin, but the top smallest k values are not
    # sorted
    smallest_elements = np.argpartition(merged_array, elements_needed)[:elements_needed]
    raveled_index_matrix_tuples = list(map(
        lambda i: unravel_merged_matrices(i, elements_per_array),
        smallest_elements
    ))
    return raveled_index_matrix_tuples


def raveled_indices_of_matrix(raveled_index_matrix_tuples:list, matrix_index:int):
    '''
    Arguments:
        raveled_index_matrix_tuples (list): a list of (raveled_index:int,
            matrix_index:int) tuples
        matrix_index (int): the matrix from which raveled_indices are requested
    Returns:
        indices (list): a list of raveled indices belonging to matrix_index
    '''
    belongs_to_matrix_q = lambda tup: tup[-1] == matrix_index
    index_extractor = lambda tup: tup[0]
    indices = list(map(index_extractor, filter(belongs_to_matrix_q, raveled_index_matrix_tuples)))
    return indices




def apply_weight_prunning(matrix:list, indices:list, inplace:bool=False):
    '''
    Arguments:
        matrix (list): a (np.ndarray) numeric matrix, assumed to have dims 2.
        indices (list): a list of indicies (corresponding to flattened matrix)
            to set to zero.
        inplace (bool): whether or not to update the passed matrix or copy it
            and return the updated copy.
    Returns:
        updated: the updated matrix where indicies are set to zero
    '''
    mat = matrix if inplace else np.copy(matrix)
    # python map doesn't work with empty list
    if len(indices) > 0:
        rows, cols = np.transpose(list(map(
            lambda i: np.unravel_index(i, mat.shape),
            indices
        )))
        mat[rows, cols] = 0
    return mat

def apply_unit_prunning(matrix:list, indices:list, inplace:bool=False):
    '''
    Arguments:
        matrix (list): a (np.ndarray) numeric matrix, assumed to have dims 2.
        indices (list): a list of indicies (corresponding to columns) to set to
            zero.
        inplace (bool): whether or not to update the passed matrix or copy it
            and return the updated copy.
    Returns:
        updated: the updated matrix where indicies are set to zero
    '''
    mat = matrix if inplace else np.copy(matrix)
    # python map doesn't work with empty list
    if len(indices) > 0:
        mat[:, indices] = 0
    return mat


def prune_k_percent(matrices:list, percent:float, method='weight_pruning'):
    '''
    Arguments:
        matrices (list): a list of numeric matrices. Each element in the list
            matrices is assumed to be a numpy.ndarray corresopnding to a weight
            matrix.

        percent (float): the percent of the smallest values to be set to zero.

        method (str / int):
            "weight_pruning" (0): sets the lowest k percent of elements in
                matrices to zero.

            "unit_pruning" (1): sets the lowest k percent of columns, based on
                the columns' l2 norm, in matrices to zero
    Returns:
        results (list):
    '''
    if method == 'unit_pruning' or method == 1:
        to_merge = [np.linalg.norm(matrix, axis=0) for matrix in matrices]
        prune_fn = apply_unit_prunning
    else:
        to_merge = matrices
        prune_fn = apply_weight_prunning

    merged, flattened_matrices, elements_per_matrix = merge_matrices(to_merge)
    number_of_indices_needed = percent_to_int(percent, merged)
    raveled_index_matrix_tuples = matrix_indices_from_merged_array(
        number_of_indices_needed, merged, elements_per_matrix
    )

    results = []
    for matrix_index, matrix in enumerate(matrices):
        indices = raveled_indices_of_matrix(raveled_index_matrix_tuples, matrix_index)
        results.append(prune_fn(matrix, indices, False))
    return results
