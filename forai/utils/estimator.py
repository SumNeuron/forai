import os, tensorflow as tf
from mag.py.os import dir_dirs
from forai.settings import EXPERIMENTS_DIR
from forai.utils.matrices import prune_method
def load_estimator_graph(export_dir:str)->None:
    '''Solves import issues when using tf.estimator.(Best)Exporter for saving
    models rather than using the last checkpoint.

    Arguments:
        export_dir (str): the full path to exported tf.estimator model
    Returns:
        None
    '''
    with tf.Session(graph=tf.Graph()) as sess:
        meta_graph   = tf.saved_model.loader.load(sess, ['serve'], export_dir)
    with tf.Session() as sess:
        loaded_graph = tf.train.import_meta_graph(meta_graph)


def lazy_fetch_variable_values(variable_names:list)->dict:
    '''
    Notes:
        "lazy" refers to:
            1. the use of `tf.initialize_all_variables()` to ensure
                variables have values
            2. the use of `tf.trainable_variables()` to search the likely
                releveant values

    Arguments:
        variable_names (list): list of variable names (str) to retrieve from the
            default tensorflow graph

    Returns:
        variables (dict): key:value of the variables and the values as pythonic
            data types.
    '''
    init_op = tf.initialize_all_variables()
    variables = {}
    with tf.Session() as sess:
        sess.run(init_op)

        tvars = tf.trainable_variables()
        tvars_vals = sess.run(tvars)

        for var, val in zip(tvars, tvars_vals):
            if var.name in variable_names:
                variables[var.name] = val
    return variables

def pruning_dir(experiment_number:int, percent:float, method:str='weight_pruning') -> str:
    '''
    Arguments:
        experiment_number (int): the number of the experiment corresponding to
            the sacred run._id.
        percent (float): the k percent prunned by the model.
        method (str / int): the method by which the model was prunned. Valid
            options include:
                0: "weight_pruning"
                1: "unit_pruning"
    '''
    return os.path.join(
        final_model_dir(experiment_number),
        '..','..',prune_method(method),'percent_{}'.format(percent)
    )

def lazy_set_variable_values(variables_to_set:dict) -> None:
    '''
    Arguments:
        variables_to_set (dict): variable_name, variable_value pairs for which
            to be updated in the graph
    '''
    init_op = tf.initialize_all_variables()
    with tf.Session() as sess:
        sess.run(init_op)
        tf_global_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)

        for var_to_find, val_to_set in variables_to_set.items():
            var = [v for v in tf_global_vars if v.name == var_to_find][0]
            sess.run(var)

            var = var.assign(val_to_set)
            sess.run(var)

def estimator_weights_names(hidden_layers:list)->list:
    '''
    Notes:
        Specific to this (forai) project.
    Arguments:
        hidden_layers (list): the list of the number of hidden units used in the
            configuration of the estimator
    Returns:
        weight_names (list): the list of the corresopnding names for the weights
            of the layers in the estimator.
    '''
    layers = ['input']+['hidden']*(len(hidden_layers)-1)+['output']
    weight_names = []
    for i, s in enumerate(layers):
        wn = '{}_layer{}/kernel:0'.format(s, '_{}'.format(i) if s == 'hidden' else '')
        weight_names.append(wn)
    return weight_names

def estimator_weights_names_for_pruning(names:list)->list:
    '''
    Notes:
        Specific to this project
    Arguments:
        names (list): a list of strings corresponding to tensorflow variables
            assumed to be the list returned by `estimator_weights_names`.
    Returns:
        names (list): a list of variables that are elegible for prunning. Namely
            excludes the output_layer
    '''
    return [n for n in names if 'output' not in names]


def available_checkpoints(experiment_number:int)->list:
    '''
    Notes:
        Specific to the use of Sacred with tf.Estimators.
        The submodule `forai.sacred` saves the model checkpoints in the same
        directory as the `sacred.FileStorageObserver` (e.g. the `run._id`)

    Arguments:
        experiment_number (int): the sacred run._id of the experiment

    Returns:
        checkpoints (list): a list of the checkpoint basenames e.g.
            `"model.chpt-<#####>"`
    '''
    get_ckpt_basename = lambda e: os.path.splitext(e)[0]
    is_ckpt_file = lambda e: 'ckpt' in e
    get_ckpt_num = lambda e: int(e.split('-')[-1])
    experiment_files = os.listdir(os.path.join(EXPERIMENTS_DIR, str(experiment_number)))
    ckpts = list(set(map(get_ckpt_basename, filter(is_ckpt_file, experiment_files))))
    ckpts.sort(key=get_ckpt_num)
    return ckpts

def get_checkpoint_files(experiment_number:int, ckpt:str):
    '''
    Notes:
        Specific to the use of Sacred with tf.Estimators.
        The submodule `forai.sacred` saves the model checkpoints in the same
        directory as the `sacred.FileStorageObserver` (e.g. the `run._id`)

    Arguments:
        experiment_number (int): the sacred run._id of the experiment
        checkpoint_basename (str): the basename of the checkpoint files to
            retrieve e.g. `"model.chpt-<#####>"`
    Returns:
        checkpoint_files (tuple): the full path to the checkpoint:
            `(data, index, meta)`
    '''
    base_dir = os.path.join(EXPERIMENTS_DIR, str(experiment_number))
    experiment_files = os.listdir(base_dir)
    is_ckpt_file = lambda e: ckpt in e
    files = list(filter(is_ckpt_file, experiment_files))
    files.sort() 
    return tuple([os.path.join(base_dir, f) for f in files])


def final_model_dir(experiment_number:int)->str:
    '''
    Notes:
        Specific to the use of Sacred with tf.Estimators.
        tf.estimator.(Best)Exporter exports the model under the specified
            directory _inside_ of a timestamp directory. Since one estimator
            will always be produced per run, this grabs the first sub directory.

    Arguments:
        experiment_number (int): the sacred run._id of the experiment

    Returns:
        directory (str) full path to the final model.
    '''
    fm_dir = os.path.join(EXPERIMENTS_DIR, str(experiment_number), 'final_model')
    return dir_dirs(fm_dir)[0]


def save_session(export_dir:str, model_name:str='estimator')->None:
    '''
    Saves the session to the desired directory

    Notes:
        Will produce the following directory and files:
            <export_dir>/
                checkpoint
                <model_name>.data-<ckpt-num>-of-<max-ckpt>
                <model_name>.index
                <model_name>.meta
    Arguments:
        export_dir (str): directory where model will be exported
        model_name (str): name of the model

    Returns:
        None
    '''
    if not os.path.isdir(export_dir): os.makedirs(export_dir)
    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())
        saver = tf.train.Saver()
        saver.save(sess, os.path.join(export_dir, model_name))
