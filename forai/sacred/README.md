# Sacred

[Sacred][sacred] is a python package for storing information about each run experiment (ex). It also provides some quality of life features for parameterizing one's
model.  It can be combined with [Ray's Tune][ray tune] hyper-parameter optimization capacity to help find the best model, and structure the results (via [Sacred][sacred]) so it is easy to browse the perhaps 1000s of models trained.

I made an example of how one might do that at the GitLab repo [ExTune][extune].

The files include:
- `experiment.py` shows how the estimator is configured and trained
- `utils.py` in this case only has the logger configuration


[sacred]: https://sacred.readthedocs.io/en/latest/quickstart.html
[ray tune]:https://ray.readthedocs.io/en/latest/tune.html
[extune]: https://gitlab.com/SumNeuron/extune
