'''----------------------------------------------------------------------------
IMPORTS
-----------------------------------------------------------------------------'''
import os, json, logging, numpy as np, tensorflow as tf

# sacred related imports
from sacred import Experiment
from sacred.observers import FileStorageObserver
from sacred.utils import apply_backspaces_and_linefeeds

# setup related imports
from forai import __name__
from forai.settings import DEFAULT_CONFIG_FILE, EXPERIMENTS_DIR
from forai.sacred.utils import config_logger

# model related imports
from forai.estimator import model_fn, input_fn, exporter, serving_input_receiver_fn

'''----------------------------------------------------------------------------
SETUP
-----------------------------------------------------------------------------'''
# define our sacred expirment (ex) and add our data_ingredient
ex = Experiment(__name__, interactive=True)
ex.add_config(DEFAULT_CONFIG_FILE)

if not os.path.isdir(EXPERIMENTS_DIR): os.makedirs(EXPERIMENTS_DIR)
ex.observers.append(FileStorageObserver.create(EXPERIMENTS_DIR))
ex.capture_out_filter = apply_backspaces_and_linefeeds

'''----------------------------------------------------------------------------
RUNTIME
-----------------------------------------------------------------------------'''
@ex.automain
def main(_log, _run, _config):
    '''
    Notes:
        variables starting with _ are automatically passed via sacred due to
        the wrapper.

        I prefer to return, at most, a single value. The returned value will be
        stored in the Observer (file or mongo) and if large weight matricies or
        the model itself, will be very inefficient for storage. Those files
        should be added via 'add_artifact' method.

    Arguments:
        _log (sacred _log): the current logger
        _run (sacred _run): the current run
        _config (sacred _config): the current configuration file

    Returns:
        result (float): accuracy if classification, otherwise mean_squared_error
    '''
    # the subdirectory for this particular experiment
    run_dir = os.path.join(EXPERIMENTS_DIR, str(_run._id))

    # inform the logger to dump to run_dir
    config_logger(run_dir)

    _log.debug('calling estimator constructor')
    estimator = tf.estimator.Estimator(
        model_fn        = model_fn,
        model_dir       = run_dir,
        config          = tf.estimator.RunConfig(**_config['RunConfig']),
        params          = _config,
        warm_start_from = None
    )

    _log.debug('setting up input functions')
    train_fn = lambda: input_fn(_config, _log)
    eval_fn  = lambda: input_fn({**_config, "mode": 'eval'}, _log)

    _log.debug('defining early stoping criteria')
    early_stop = tf.contrib.estimator.stop_if_no_decrease_hook(
        estimator,
        _config['stop_if_no_decrease_metric'],
        _config['stop_if_no_decrease_steps']
    )

    train_spec = tf.estimator.TrainSpec(
        input_fn=train_fn,
        max_steps=_config['train_max_steps'],
        hooks=[early_stop]
    )

    eval_spec  = tf.estimator.EvalSpec(
        input_fn=eval_fn,
        exporters=exporter
    )


    _log.debug('begin training, logs mostly handled by TensorFlow at this point')
    trained = tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)

    if _config['save']:
        _log.debug('exporting the final model')
        estimator.export_savedmodel(os.path.join(run_dir, 'final_model'), serving_input_receiver_fn)

    return trained[0]
