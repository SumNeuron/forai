# forai
This project has been structured as a python module, with three sub-modules.
A high level overview will be provided here as sub-module readmes in conjunction with ample docstrings and inline documentation should cover the rest.


- `estimator`: this sub-module handles all the bells and whistles of using the `tf.estimator` API. While overboard for this toy example, one can easily see where and how to make far more sophisticated architectures with niche losses, etc.
- `sacred`: this sub-module handles _implementing_ the `tf.estimator` API i.e. the `estimator` sub-module defines what the estimator should be, whereas the this sub-module initializes it, adds all the quality of life hooks (e.g. early stopping) and trains it
- `utils`: this sub-module is mostly implement in the accompanying files under `<repo>/jupyter/`. This includes functions for reading, updating, and writing of trained `tf.estimators` as well as the different pruning methods
- `config.json`: this is the default configuration for running a Sacred experiment. As such only these values can be overridden at run time.
- `settings.py`: in this case this just has some useful paths for knowing where to read and write files

This library is not published to pypi, however one can install it locally via:
```
# from /path/to/<repo>
pip install -e ./
```
