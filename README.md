# Weight Pruning
Of note are two sub-directories:
- `forai`: python module handling implementation of code needed for weight pruning.
- `jupyter`: a collection of jupyter notebooks for running the code interactively.

Each sub-directory has an associated README for further clarification.

To run the jupyter-notebooks interactively this module will need to be installed locally via:

1. clone the repository
2. `cd <path-to>/forai`
3. `pip install -e ./`
